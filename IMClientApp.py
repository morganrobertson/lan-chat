import threading
import queue
import time
import tkinter
from tkinter import *
from tkinter import ttk
from IMClient import IMClient

class IMClientApp(ttk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.root = root
        self.root.title('Chat Client')
        self.grid(row=0, column=0, sticky=(N, E, S, W))

        self.conversation = StringVar()
        self.username = StringVar()
        self.to = StringVar()
        self.message = StringVar()

        self.usernameLbl = ttk.Label(self, text='Username')
        self.usernameLbl.grid(row=0, column=0, sticky=E)

        self.usernameEnt = ttk.Entry(self, textvariable=self.username)
        self.usernameEnt.grid(row=0, column=1, sticky=W)

        self.usernameBtn = ttk.Button(self, text='Set', command=self.setUsername)
        self.usernameBtn.grid(row=0, column=3)

        self.conversationBox = Text(self, width=40, height=10)
        self.conversationBox.grid(row=1, column=1)

        self.toLbl = ttk.Label(self, text='To')
        self.toLbl.grid(row=2, column=0, sticky=E)

        self.toEnt = ttk.Entry(self, textvariable=self.to)
        self.toEnt.grid(row=2, column=1, sticky=W)

        self.messageLbl = ttk.Label(self, text='Message')
        self.messageLbl.grid(row=3, column=0, sticky=E)

        self.messageEnt = ttk.Entry(self, textvariable=self.message, width=40)
        self.messageEnt.grid(row=3, column=1, sticky=W)

        self.sendBtn = ttk.Button(self, text='Send', command=self.send)
        self.sendBtn.grid(row=4, column=1, sticky=W)

        for child in self.winfo_children():
            child.grid_configure(padx=8, pady=8)

        self.usernameEnt.focus()

        self.imClient = None

        threading.Thread(target=self.checkForChatMessages).start()
        
    def setUsername(self, *args):
        self.imClient = IMClient(self.username.get())
        threading.Thread(target=self.imClient.start).start()

    def send(self, *args):
        if self.imClient is not None:
            self.imClient.chatMessagesOut.put((self.to.get(), self.message.get()))
            output = '{}: {}\n'.format(self.username.get(), self.message.get())
            self.conversationBox.insert(END, output)

    def checkForChatMessages(self):
        while True:
            try:
                if self.imClient is not None:
                    sender, msg = self.imClient.chatMessagesIn.get(block=False)
                    output = '{}: {}\n'.format(sender, msg)
                    self.conversationBox.insert(END, output)
                else:
                    time.sleep(0.1)
            except queue.Empty as e:
                time.sleep(0.1)


if __name__ == '__main__':
    root = tkinter.Tk()
    IMClientApp(root)
    root.mainloop()
