import sys
import select
import time
import threading
import queue
from queue import Queue
import socket


class UDPListenerTask(threading.Thread):
    def __init__(self, servername, udpPort=8001, tcpPort=8002):
        super().__init__()
        self.servername = servername
        self.udpPort = udpPort
        self.tcpPort = tcpPort

    def run(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        s.bind(('', self.udpPort))

        while True:
            data = None
            address = None

            try:
                data, address = s.recvfrom(256)
                response = '@server:{}:{}'.format(self.servername, self.tcpPort)
                if data is not None and data.decode().startswith('@client:'):
                    s.sendto(response.encode(), address)
                    print('Pinged by: {}, {}'.format(data, address))
            except Exception:
                print('Exception occurred in UDPListenerTask.run()')

class HandleClientTask(threading.Thread):
    def __init__(self, clientSock, clientAddr, servername, clients, clientDictMutex):
        super().__init__()
        self.clientSock = clientSock
        self.clientSockMutex = threading.Lock()
        self.clientAddr = clientAddr
        self.clientID = None
        self.servername = servername
        self.clients = clients
        self.clientDictMutex = clientDictMutex
        self.inbox = Queue()
        self.outbox = Queue()
        self.threads = []
        self.continueRunning = True

    def run(self):
        # Get client username
        gotID = False
        while not gotID:
            msg = '#REQ_CLIENT_ID;@server:{}'.format(self.servername).encode()
            response = None
            with self.clientSockMutex:
                try:
                    self.clientSock.sendall(msg)
                    response = self.clientSock.recv(256).decode()
                except Exception as e:
                    print('In HandleClientTask.__init__:', e)
            if response is not None and response.startswith('$RES_CLIENT_ID'):
                fields = response.split(';')
                for field in fields:
                    if field.startswith('@client:'):
                        self.clientID = field
                        gotID = True
                        with stdoutMutex:
                            print('ClientID:', self.clientID)
                        break

        # Add this client to clients dictionary
        with self.clientDictMutex:
            self.clients[self.clientID] = self
        
        self.threads.append( threading.Thread(target=self.listenForMessages) )
        self.threads.append( threading.Thread(target=self.sendMessages) )
        self.threads.append( threading.Thread(target=self.processMessages) )

        for thread in self.threads:
            thread.start()

        for thread in self.threads:
            thread.join()

        self.clientSock.close()
        
        with stdoutMutex:
            print('Closing connection to', self.clientAddr)

    def listenForMessages(self):
        
        '''
        Listen for incoming messages from the client and thro received
        data into the inbox for processing

        Returns:
            None
        '''
        
        while self.continueRunning:
            try:
                ready = select.select([self.clientSock], [], [], 0)
                if ready[0]:
                    data = self.clientSock.recv(256)
                    
                    if len(data) > 0:
                        with stdoutMutex:
                            print('Received:', data)
                        self.inbox.put(data)
                    else:
                        # Lost connection with client
                        self.continueRunning = False
                        with stdoutMutex:
                            print('Connection lost with', self.clientID)
                        break
                else:
                    time.sleep(0.1)
            except socket.error as e:
                self.continueRunning = False
                with stdoutMutex:
                    print('Exception in listenForMessages:', e)
            

    def sendMessages(self):
        while self.continueRunning:
            try:
                data = self.outbox.get(block=False)
            except queue.Empty as e:
                time.sleep(0.1)
            else:
                with stdoutMutex:
                    print('Sending:', data)
                self.clientSock.sendall(data)
        with stdoutMutex:
            print('Exiting sendMessages()')

    def processMessages(self):
        '''
        Pop messages from the inbox queue, process them, and put a message in the
        outbox queue if necessary
        '''
        
        inMsg = None
        while self.continueRunning:
            try:
                inMsg = self.inbox.get(block=False).decode()
            except queue.Empty as e:
                time.sleep(0.1)
                pass
            else:
                fields = inMsg.split(';')

                if len(fields) > 0:
                    messageType = fields[0]
                    client = None
                    outMessage = None

                    if messageType == '#MSG':
                        client, outMessage = self.processMSG(fields[1:])

                    '''
                    # Put message in outbox
                    if client is not None and outMessage is not None:
                        self.outbox.put((client, outMessage))
                    '''

    def processMSG(self, fields):

        '''
        Process a received chat message

        Fields:
            1: @client:<from>
            2: @client:<to>
            3: <message>

        Adds a 2-tuple containing (<from>, <msg>) to the chatMessagesIn queue
        '''

        client = None
        msg = None

        if len(fields) >= 3:
            fromField = fields[0]
            toField = fields[1]
            msgField = fields[2]

            with self.clientDictMutex:
                client = self.clients[toField]
                
            msg = ';'.join(['#MSG;@server:{}'.format(self.servername), fromField, toField, msgField])

            client.outbox.put(msg.encode())
            
        return None, None
                
class IMServer:
    def __init__(self, servername):
        self.servername = servername
        self.tcpPort = 8002
        self.clients = {}
        self.clientDictMutex = threading.Lock()

    def start(self):
        udpThread = UDPListenerTask(servername, tcpPort=self.tcpPort)
        udpThread.start()

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('', self.tcpPort))
        sock.listen(8)

        while True:
            connection, address = sock.accept()
            with stdoutMutex:
                print('Connected:', address)
            serverID = '{}:{}'.format(self.servername, self.tcpPort)
            clientThread = HandleClientTask(connection, address, serverID, self.clients, self.clientDictMutex)
            clientThread.start()

stdoutMutex = threading.Lock()
if __name__ == '__main__':
    servername = sys.argv[1] if len(sys.argv) > 1 else 'python'
    server = IMServer(servername)
    server.start()
