import sys
import select
import time
import threading
import queue
from queue import Queue
from socket import *

class Server:
    
    '''
    Stores information about a remote server to which an IMClient can connect
    '''
    
    def __init__(self, serverID, serverIP):
        self.serverID = serverID
        self.IP = serverIP
        self.connection = None
        self.isConnected = False
        self.lock = threading.Lock()

        # Parse server port from ID string
        # serverID format should be @server:<servername>:<port>
        fields = serverID.split(':')
        if len(fields) > 2 and fields[2].isdecimal():
            self.port = int(fields[2])
        else:
            self.port = 8002
            

class IMClient:
    def __init__(self, clientName):
        self.clientName = clientName
        self.servers = None
        self.inbox = Queue()  # contains bytes objects
        self.outbox = Queue()  # contains (server, bytes) tuples
        self.chatMessagesIn = Queue()
        self.chatMessagesOut = Queue()
        self.threadLock = threading.Lock()
        self.continueRunning = True
        self.threads = []

    def start(self):
        
        # Search for servers on the local network
        servers = self.findServers()
        with stdoutMutex:
            self.servers = servers

        self.threads.append( threading.Thread(target=self.processMessages) )
        self.threads.append( threading.Thread(target=self.sendMessages) )
        self.threads.append( threading.Thread(target=self.processChatMessagesOut) )

        # Change: currently connects to first server found
        if len(servers) > 0:
            server = list(self.servers.values())[0]
            self.connectToServer(server)
            self.threads.append( threading.Thread(target=self.listenForMessages, args=(server,)) )

        for thread in self.threads:
            thread.start()

    def stop(self):
        self.continueRunning = False

        for thread in self.threads:
            thread.join()

        for server in self.servers.values():
            if server.isConnected:
                server.connection.close()

    def listenForMessages(self, server):

        '''
        Listen for incoming messages from a given server and throw received
        data into the inbox queue for processing

        Parameters:
        server      the server to listen to

        Returns:
            None
        '''
        
        with stdoutMutex:
            print('connection type:', type(server.connection))
        
        while self.continueRunning:
            try:
                ready = select.select([server.connection], [], [], 0)
                if ready[0]:
                    data = server.connection.recv(256)

                    if len(data) > 0:
                        with stdoutMutex:
                            print('inbox:', data)
                        self.inbox.put(data)
                    else:
                        # Lost connection with server
                        self.continueRunning = False
                        with server.lock:
                            server.isConnected = False
                        break
                else:
                    time.sleep(0.1)
            except socket.error as e:
                # Change
                self.continueRunning = False
                with stdoutMutex:
                    print('Exception in listenForMessages():', e)

    def sendMessages(self):

        '''
        Pop messages from the outbox queue and send them to the appropriate server
        '''

        while self.continueRunning:
            try:
                server, msg = self.outbox.get(block=False)
                with stdoutMutex:
                    print('outbox:', msg)
                    print('to:', server.IP)
                if server.isConnected:
                    with stdoutMutex:
                        print('sending:', msg.encode())
                    server.connection.sendall(msg.encode())
            except queue.Empty as e:
                time.sleep(0.1)

    def processMessages(self):

        '''
        Pop messages from the inbox queue, process them, and put a response in the
        outbox queue if necessary
        '''

        inMsg = None
        while self.continueRunning:
            try:
                inMsg = self.inbox.get(block=False).decode()
                with stdoutMutex:
                    print('from inbox got:', inMsg)
            except queue.Empty as e:
                time.sleep(0.1)
            else:
                fields = inMsg.split(';')

                if len(fields) > 0:
                    messageType = fields[0]
                    server = None
                    response = None

                    # Process the message according to its type
                    if messageType == '#REQ_CLIENT_ID':
                        server, response = self.processREQ_CLIENT_ID(fields[1:])
                    elif messageType == '#MSG':
                        server, response = self.processMSG(fields[1:])

                    # Put response in outbox
                    if server is not None and response is not None:
                        with stdoutMutex:
                            print('inbox:', server, response)
                        self.outbox.put((server, response))

    def processREQ_CLIENT_ID(self, fields):

        '''
        Process a request to a client to identify itself

        Fields:
            0: @server:<servername>
        '''

        for field in fields:
            with stdoutMutex:
                print('field:', field)

        server = None
        response = None

        if len(fields) >= 1:
            fromField = fields[0]
        
            if fromField.startswith('@server:'):
                with self.threadLock:
                    server = self.servers[fromField]

                response = '$RES_CLIENT_ID;@client:{}'.format(self.clientName)

            with stdoutMutex:
                print('response:', response)
            
        return server, response

    def processMSG(self, fields):

        '''
        Process a received chat message

        Fields:
            0: @server:<servername>
            1: @client:<from>
            2: @client:<to>
            3: <message>

        Adds a 2-tuple containing (<from>, <msg>) to the chatMessagesIn queue
        '''

        if len(fields) >= 4:
            fromField = fields[1]
            msgField = fields[3]
            self.chatMessagesIn.put((fromField, msgField))

            with stdoutMutex:
                print('CHAT MSG', fromField, msgField)

        return None, None

    def processChatMessagesOut(self):

        '''
        Pop messages from the chatMessagesOut queue, process them,
        and put them in the outbox
        '''

        msg = None
        to = None
        serverName = None
        while self.continueRunning:
            try:
                to, msg = self.chatMessagesOut.get(block=False)
            except queue.Empty as e:
                time.sleep(0.1)
            else:
                messageOut = '#MSG;@client:{};@client:{};{}'.format(self.clientName, to, msg)
                # Change
                self.outbox.put((list(self.servers.values())[0], messageOut))
    
    def findServers(self):

        '''
        Find servers on the local network via a UDP broadcast

        Returns:
            A dictionary whose keys are server ID strings and whose values are
            server objects
        '''
        
        s = socket(AF_INET, SOCK_DGRAM)
        s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

        ipAddressList = gethostbyname_ex(gethostname())[2]  # Get list of IP addresses for local machine
        identifier = '@client:{}'.format(self.clientName)

        servers = {}
        for IP in ipAddressList:
            broadcastIP = inet_aton(IP)[:3] + b'\xff'  # i.e. 192.168.1.255
            broadcastAddr = (inet_ntoa(broadcastIP), 8001)
            s.sendto(identifier.encode(), broadcastAddr)

            # Read response from all responding servers
            while True:
                ready = select.select([s], [], [], 1)
                if ready[0]:
                    serverID, addr = s.recvfrom(128)

                    if serverID is not None and addr is not None:
                        server = Server(serverID.decode(), addr[0])

                        # Don't overwrite server object if multiple servers have the same name
                        serverKey = serverID.decode()
                        n = 1
                        while serverKey in servers:
                            serverKey = '{}({})'.format(str(serverID),n)
                            n += 1

                        print('Found:{}:{}'.format(serverKey, server.serverID))
                        servers[serverKey] = server
                else:
                    break

        return servers

    def connectToServer(self, server):
        server.connection = socket(AF_INET, SOCK_STREAM)
        server.connection.connect((server.IP, server.port))
        server.isConnected = True

stdoutMutex = threading.Lock()
if __name__ == '__main__':
    clientName = sys.argv[1] if len(sys.argv) > 1 else 'python'
    client = IMClient(clientName)
    client.start()
    time.sleep(3)
    client.stop()

    '''
    servers = findServers(clientName)

    for server in servers:
        serverID, serverIP = server
        print('\nserverID: {}\nserverIp: {}'.format(server[0], serverIP))
        s = connectToServer(serverIP, 8002)
        s.sendall('Hello world!'.encode())
        data = s.recv(32)
        print('Server response:', data)
        time.sleep(5)
        s.close()
    '''
